import Action from './action.vue';
import Apps from './apps.vue';
import Brand from './brand.vue';
import Setting from './setting.vue';
import Status from './status.vue';
import Toolbar from './toolbar.vue';
import Workspace from './workspace.vue';

export { Action, Apps, Brand, Setting, Status, Toolbar, Workspace };
