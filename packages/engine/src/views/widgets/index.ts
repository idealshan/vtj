import Empty from './Empty.vue';
import Logo from './Logo.vue';
import Dependencies from './Dependencies.vue';
import Designer from './Designer.vue';
import Components from './Components.vue';
import Pages from './Pages.vue';
import Blocks from './Blocks.vue';
import Toolbar from './Toolbar.vue';
import Outline from './Outline.vue';
import Properties from './Properties.vue';
import Events from './Events.vue';
import BlockScript from './BlockScript.vue';
import Css from './Css.vue';
import Schema from './Schema.vue';
import Actions from './Actions.vue';
import Directives from './Directives.vue';
import Advanced from './Advanced.vue';
import Switcher from './Switcher.vue';
import Docs from './Docs.vue';
import Apis from './Apis.vue';
import DataSources from './DataSources.vue';
import History from './History.vue';
import NodePath from './NodePath.vue';
import About from './About.vue';

export const buildInWidgets = {
  Empty,
  Logo,
  Dependencies,
  Designer,
  Components,
  Pages,
  Blocks,
  Toolbar,
  Outline,
  Properties,
  Events,
  BlockScript,
  Css,
  Schema,
  Actions,
  Directives,
  Advanced,
  Switcher,
  Docs,
  Apis,
  DataSources,
  History,
  NodePath,
  About
};
